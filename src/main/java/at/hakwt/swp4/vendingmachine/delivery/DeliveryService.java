package at.hakwt.swp4.vendingmachine.delivery;

import at.hakwt.swp4.vendingmachine.model.Drink;

import java.util.List;

public interface DeliveryService {

    void deliver(List<Drink> drinks);

}
