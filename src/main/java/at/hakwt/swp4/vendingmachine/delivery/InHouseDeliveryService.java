package at.hakwt.swp4.vendingmachine.delivery;

import at.hakwt.swp4.vendingmachine.model.Drink;

import java.util.List;

public class InHouseDeliveryService implements DeliveryService {
    @Override
    public void deliver(List<Drink> drinks) {
        System.out.println("Delivering " + drinks.size() + " drinks: " + drinks.get(0).getName());
    }
}
